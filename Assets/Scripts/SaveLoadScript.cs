﻿//Primarily using this for documentation:
//http://wiki.unity3d.com/index.php?title=Saving_and_Loading_Data:_XmlSerializer

using UnityEngine;

using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("SaveLoadScript")]
public class SaveLoadScript {

	public Progress progress;

	//Array called Levels of individual Level
	[XmlArray("Toggles")]
	[XmlArrayItem("Toggle")]
	public SwitchToggle[] Toggles;

	//Array called Levels of individual Level
	[XmlArray("Levels")]
	[XmlArrayItem("Level")]
	public string[] Levels;

	//Switch Toggle Class that holds name and flipped state
	public class SwitchToggle {
		[XmlAttribute("name")]
		public string name;

		[XmlAttribute("flipped")]
		public bool flipped;
	}
		
	//Level Class for Level ifnormation
	public class Level {
		[XmlAttribute("name")]
		public string name;

		public bool completed;

		public int steps;
	}

	//Holds information on Progress
	public class Progress {
		public string CurrentDungeon;
		public string CurrentSection;
		public string CurrentLevel;
	}
}
