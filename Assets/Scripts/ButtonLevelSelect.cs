﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class ButtonLevelSelect : MonoBehaviour {

	//Game Controller
	public GameControllerScript gameControllerScript;

	// Use this for initialization
	void Start () {
		gameControllerScript = GameObject.Find ("GameController").GetComponent<GameControllerScript> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void levelLoad() {
		string buttonText = gameObject.GetComponentInChildren<Text>().text;

		gameControllerScript.gameObject.GetComponent<levelBuild>().levelCompleted(Resources.Load("LevelCSVs/" + buttonText) as TextAsset);

	}
}
