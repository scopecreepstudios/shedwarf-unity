﻿using UnityEngine;
using System.Collections;

public class ToggleableObjectScript : MonoBehaviour {

	public string toggleName;

	public void toggleRemoves(string broadcastToggleName) {
		if(broadcastToggleName == toggleName) {
			StartCoroutine(destroyBrick());
		}
	}

	public void toggleMoves(string broadcastToggleName) {
		
	}

	public void toggleChanges(string broadcastToggleName) {
		
	}

	public IEnumerator destroyBrick() {

		//get rid of the collider
		Destroy(gameObject.GetComponent<BoxCollider>());

		float rateToReduce = .1f;
		while(true) {
			this.gameObject.transform.localScale -= new Vector3(rateToReduce,rateToReduce,rateToReduce);

			if(this.gameObject.transform.localScale.x <= 0f) {
				break;
			}

			yield return new WaitForSeconds(.001f);
		}

		//Make it go away forever
		Destroy(this.gameObject);
	}
}
