﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;
using DarkTonic.MasterAudio;
using System.IO;

public class DoorScript : MonoBehaviour {

	//Main Game Controller Script
	public GameControllerScript gameControllerScript;

	public TextAsset nextLevelTextAsset;
    
	// Use this for initialization
	void Start () {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	public void setNextLevelTextAsset(string nameOfTextAsset) {
		nextLevelTextAsset = Resources.Load("LevelCSVs/" + nameOfTextAsset) as TextAsset;
	}
    
    public void openDoor() {
		gameObject.GetComponent<BoxCollider> ().isTrigger = true;
		gameObject.GetComponent<levelObject> ().objectType = "OpenDoor";
    }
    
    public void OnTriggerEnter(Collider other) {
		//When She Dwarf Enters the Door we'll let Level Finished know we're done
		if (other.gameObject.GetComponent<levelObject>().name == "SheDwarf") {
			//Fire off the Custom Event
			MasterAudio.FireCustomEvent("lockCubeOpen", gameObject.transform.position);

			StartCoroutine(gameControllerScript.levelEnd (nextLevelTextAsset));
        }
    }
}
