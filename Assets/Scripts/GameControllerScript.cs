﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

//Need this for showing list of levels
using System.IO;

// //Use this for determining directions for objects. Used in a lot of places
public enum Direction {
	left,
	right,
	up,
	down,
	none
}

//Determines which turn it is in the game
//Roughly speaking:
//Level starts and does what it needs to
//Game loop starts then
//Then She Dwarf has a start and end of turn
//Then Enemies have a start and End
//That loops back through until the level is done
//Level end
//Alternatively, if she dwarf dies we do a game over
public enum GameTurn {
    StartLevel,
	SheDwarfStart,
    SheDwarfEnd,
	EnemyStart,
    EnemyEnd,
    LevelEnd,
	SheDwarfDied,
    GameOver
}

public class GameControllerScript : MonoBehaviour {

	//We'll use this to determine the basic grid size that will gets 
	//Hidden so it's less likely to get messed with
	[HideInInspector] public int gridSize;
	[HideInInspector] public PlayerProgressScript playerProgress;

	[Header("Game Information")]
	//Used for managing what Turn we're on
	public GameTurn currentTurn;

	//Will be used for storing/displaying move counts
	//TODO moves will need to be stored on a per level basis in a progress file
	public Text movesText;
	public int numberOfMoves;

	[Header("D/S/L Data")]
	public string currentDungeon;
	public string currentSection;
	public string currentLevel;

	[Header("Text Outlets")]
	public Text levelText;
	public Text currentTurnText;

	void Awake () {
		//Set Grid Size. If this changes we'll need to update a lot of stuff
		//Base as much math as possible on this to keep things consistent
		//Might move this into a separate file as a constant for easier referencing
		gridSize = 32;

		//When we start up we need to set up the current level we're on
		currentTurn = GameTurn.StartLevel;
		playerProgress = gameObject.GetComponent<PlayerProgressScript>();
	}

	void Update() {
		currentTurnText.text = currentTurn.ToString ();
	}
    
	//Gets called from levelBuild when it's done building the level
	public void buildLevelComplete(TextAsset levelLoaded) {
		//Track level information
		currentLevel = levelLoaded.name;

		//Split up the text asset so we can pull out the level information
		string[] splitUpText = levelLoaded.text.Split(',');

		//Update Save Data for Dungeon
		playerProgress.currentDungeonUpdate(splitUpText[0]);
		currentDungeon = playerProgress.currentDungeonLoad();

		//Updat Save Data for Section
		playerProgress.currentSectionUpdate(splitUpText[1]);
		currentSection = playerProgress.currentSectionLoad();

		//Update Save Data for Level
		playerProgress.currentLevelUpdate(levelLoaded.name);
		levelText.text = playerProgress.currentLevelLoad();

		//Save those changes
		playerProgress.Save();
		//Set it to shedwarf's turn
		currentTurn = GameTurn.SheDwarfStart;

		//Tell her she can get going
		GameObject.Find ("SheDwarf").GetComponent<sheDwarfMove> ().sheDwarfTurnStart ();
    }

	//Once we're in the primary game loop let's use this to advance to She Dwarf's turn
	//Need to leave buildLevelComplete() as separate in case we need to do more there
	public void sheDwarfTurnStart() {
//		Debug.Log ("She Dwarf Turn Start");
		//Set it to her turn
		currentTurn = GameTurn.SheDwarfStart;
		//Tell her she can get going
		GameObject.Find ("SheDwarf").GetComponent<sheDwarfMove> ().sheDwarfTurnStart ();
	}

	//After She Dwarf is done doing her action
	//She Dwarf will call this directly from her script
	public void sheDwarfTurnEnd() {
		//If it is the Level End Let's do something else
		//She Dwarf will finish her movement into the final square
		//Make sure we don't trigger if we've already moved to Level End
		if (currentTurn == GameTurn.LevelEnd) {
			
		} 
		//Otherwise the level is still going
		else {
			//Do anything we should do at the end of her turn
			currentTurn = GameTurn.SheDwarfEnd;	
			//Check for a cleared board/end game condition
			checkForClearBoard ();

			//Move on to the Enemy Start turn. Set it:
			currentTurn = GameTurn.EnemyStart;
			//Tell Enemy Controller that it can let the enmies do their thing
			StartCoroutine (gameObject.GetComponent<EnemyController> ().enemiesAct ());
		}
	}

	public void enemyActionsDone() {
		//Enemies are all done:
		currentTurn = GameTurn.EnemyEnd;
		//If She Dwarf is dead we need to go to game over
		if (GameObject.Find ("SheDwarf").GetComponent<sheDwarfMove> ().alive == false) {
			sheDwarfDied ();	
		} 
		else {
			sheDwarfTurnStart ();
		}
	}

	//Called from DoorScript when the level is ended by shedwarf completing her goal
	public IEnumerator levelEnd(TextAsset nextLevelTextAsset) {
		currentTurn = GameTurn.LevelEnd;

		//Do anything final level things here
		//Make sure we let all the animations finish up
		yield return new WaitForSeconds (1f);

		//Build the level
		gameObject.GetComponent<levelBuild>().levelCompleted(nextLevelTextAsset);
	}
    
	//TODO make this check what we should check for level completition
	public void checkForClearBoard() {
		if (GameObject.FindGameObjectWithTag ("Grublin") == null && GameObject.FindGameObjectWithTag ("Owl") == null && GameObject.FindGameObjectWithTag ("Mummy") == null) {
            GameObject[] doors = GameObject.FindGameObjectsWithTag("Door");
            for(int i = 0; i < doors.Length; i++) {
                doors[i].GetComponent<DoorScript>().openDoor();
            }
		}
	}
    
	public void sheDwarfDied() {
		currentTurn = GameTurn.SheDwarfDied;
		//Do game over stuff
		StartCoroutine(gameOver());
		currentTurn = GameTurn.GameOver;
	}

	public IEnumerator gameOver() {
		Debug.Log("GAME OVER");
		yield return true;
	}
		
	public void restartLevel() {
		//Builds based on the text within the textarea
		//Used for testing

		TextAsset restartLevelTextAsset = Resources.Load("LevelCSVs/" + GameObject.Find("Level").GetComponent<Text>().text) as TextAsset;
		gameObject.GetComponent<levelBuild>().levelCompleted(restartLevelTextAsset);
	}

	//Called when we need to quit the game
	public void quitGame() {
		Application.Quit();
	}

	//Add to the number of moves and update the text on the screen
	public void addMovementNumber() {
		numberOfMoves += 1;
		movesText.text = "Moves: " + numberOfMoves.ToString();
	}    



	[Header("Level List Whatnot")]

	public GameObject levelList;
	public GameObject levelListLayoutGroup;

	public GameObject levelSelectButton;


	public void levelListShowHide(Toggle toggleState) {

		if(toggleState.isOn) {
			//Show the LevelList and LevelListLayoutGroup
			levelList.SetActive(true);
	

			//Find the directory (can be hardcoded)

			DirectoryInfo levelDirectoryPath = new DirectoryInfo(Application.dataPath);

			FileInfo[] fileInfo = levelDirectoryPath.GetFiles("*.csv", SearchOption.AllDirectories);

			foreach (FileInfo file in fileInfo) {
				GameObject newButton = Instantiate (levelSelectButton) as GameObject;
				newButton.transform.parent = levelListLayoutGroup.transform;
				newButton.GetComponentInChildren<Text>().text = file.Name.Substring(0, file.Name.Length - 4);;
				Debug.Log(file);
			}

			//Get a list of all the files

			//Foreach file make an object of the LevelSelectButton class

			//Set that button as a child of the LevelListLayoutGroup

			//Set the text of the LevelSelectButton to the file name	
		}
		else {
			ButtonLevelSelect[] buttons = GameObject.FindObjectsOfType<ButtonLevelSelect>();
			foreach(ButtonLevelSelect button in buttons) {
				Destroy(button.gameObject);
			}
			levelList.SetActive(false);
		}


	}
}