﻿using UnityEngine;
using System.Collections;

public class MummyScript : MonoBehaviour {

	//Rest turn before attack
	public bool resting;

	//Death Variables
	public bool alive = true;
	public float timeOfDeathStart;
	public float timeForFullDeath = 1.0f;
	public float speed = 4f;

	//Main Game Controller Script
	[HideInInspector] public GameControllerScript gameControllerScript;

	// Use this for initialization
	void Start () {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
		resting = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//Moves towards She Dwarf
	public void mummyAction() {
//		Debug.Log ("Mummy Action" );
		if (!alive) {
			death ();
		}

		if (!resting && alive) {
			//Determine which direction shedwarf is in and where you'll move
			Direction directionToMove = whereIsSheDwarf ();

			//Shoot rays in direction based on where shedwarf is. This'll determine if the way is clear
			if (shootRays (directionToMove)) {
				moveDirection (directionToMove);
			}

			resting = true;
		} 
		else {
			resting = false;
		}
	}

	void death() {
//		alive = true;
		Destroy (gameObject);
	}


	public void moveDirection(Direction directionToMove) {

		switch (directionToMove) {
		case Direction.up:
			StartCoroutine (moveEnemy (gameObject.transform.position, gameObject.transform.position + new Vector3 (0,0,gameControllerScript.gridSize)));
			break;
		case Direction.down:
			StartCoroutine (moveEnemy (gameObject.transform.position, gameObject.transform.position + new Vector3 (0,0,-1 * gameControllerScript.gridSize)));
			break;
		case Direction.left:
			StartCoroutine (moveEnemy (gameObject.transform.position, gameObject.transform.position + new Vector3 (-1 *gameControllerScript.gridSize,0,0)));
			break;
		case Direction.right:
			StartCoroutine (moveEnemy (gameObject.transform.position, gameObject.transform.position + new Vector3 (gameControllerScript.gridSize,0,0)));
			break;
		case Direction.none:
			break;
		}
	}

	//Handles moving the player from one spot to the next
	public IEnumerator moveEnemy(Vector3 startPos, Vector3 endPos) {

		//Current Position should start at start position and will slowly move towards end position
		Vector3 currentPos = gameObject.transform.position;

		//When does all this start?
		float movementStart = Time.time;

		//Move from point A to point B in increments of 1 pixel
		while(currentPos != endPos) {
			//Recheck this every frame
			currentPos = gameObject.transform.position;

			Vector3 updatedPos = Vector3.Lerp (startPos, endPos, (Time.time - movementStart) * 10);
			gameObject.transform.position = new Vector3(Mathf.Round(updatedPos.x), Mathf.Round(updatedPos.y), Mathf.Round(updatedPos.z));
			yield return null;
		}
		yield return true;
	}



	//looks between object and shedwarf and determines the best direction to move in
	public Direction whereIsSheDwarf() {
		GameObject sheDwarf = GameObject.FindGameObjectWithTag ("Player");

		//Finds angle between two positions. Found this on the internet bc I don't get geometry
		float angleBetween = Mathf.Atan2(sheDwarf.transform.position.z-gameObject.transform.position.z, sheDwarf.transform.position.x-gameObject.transform.position.x)*180 / Mathf.PI;
//		Debug.Log (angleBetween);
		if (angleBetween > -45 && angleBetween < 45) {
			return Direction.right;
		} 
		else if (angleBetween >= 45 && angleBetween <= 135) {
			return Direction.up;
		} 
		else if ((angleBetween >= 135 && angleBetween <= 180) || (angleBetween <= 0 && angleBetween <= -135)) {
			return Direction.left;
		}
		else if (angleBetween >= -135 && angleBetween <= -45) {
			return Direction.down;
		} 
		else {
			return Direction.none;
		}
	}

	//If within striking distance attack shedwarf
	public void attackSheDwarf(GameObject sheDwarf) {
		sheDwarf.transform.GetComponent<sheDwarfMove> ().killedByEnemy ();
	}

	//Check direction to see if you can move or not
	bool shootRays(Direction directionToCheck) {
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
			gameObject.transform.position.y+8, 
			gameObject.transform.position.z);

		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;
		string hitObjectType = "";

		//Depending on which direction to move grab the appproriate Vector direction
		Vector3 raycastTransformDirection;
		switch (directionToCheck) {
			case Direction.left:
				raycastTransformDirection = transform.TransformDirection (Vector3.left);
				break;
			case Direction.right:
				raycastTransformDirection = transform.TransformDirection (Vector3.right);
				break;

			case Direction.up:
				raycastTransformDirection = transform.TransformDirection (Vector3.forward);
				break;

			case Direction.down:
				raycastTransformDirection = transform.TransformDirection (-Vector3.forward);
				break;
			default:
				raycastTransformDirection = transform.TransformDirection (-Vector3.forward);
				break;
		}

		//Raycast out a gridSize length in the approprate Vector
		if (Physics.Raycast (currentPos, raycastTransformDirection, out hit, gameControllerScript.gridSize)) {
			hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
		}

		if (hitObjectType != "") {
//			Debug.Log (hitObjectType);
		}

		if (hitObjectType == "Wall" ||
			hitObjectType == "Chest" ||
			hitObjectType == "Brick" ||
			hitObjectType == "Grublin" ||
			hitObjectType == "Owl" ||
			hitObjectType == "Skull" ||
			hitObjectType == "Pushable" ||
			hitObjectType == "Mummy") {
//			endTurn ();
			return false;
		} else if (hitObjectType == "SheDwarf") {
			attackSheDwarf (hit.transform.gameObject);
			return true;
		} 
		else {
			return true;
		}
	}
}