﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class GrublinScript : MonoBehaviour {
		
	//Death Variables
	public bool alive = true;
	public float timeForFullDeath = 1.0f;

	//Did you kill Shedwarf? Good for you!
	public bool victoryGrow = false;
	public float timeOfVictoryStart;
	public float timeForFullVictory = 1.0f;

	//Facing variables
	//This will get set when the level is built
	public Direction grublinFacingDirection = Direction.up;

	//Should this Grublin rotate counterclockwise
	public bool counterClockwise = false;

	//Game Controller
	public GameControllerScript gameControllerScript;

	//Sprite
	public AnimateSprite spriteToAnimate;

	// Use this for initialization
	void Start () {
		gameControllerScript = GameObject.Find ("GameController").GetComponent<GameControllerScript> ();
	}

	public void setStartUpDirectionAnimation() {
		spriteToAnimate.updateCycle(grublinFacingDirection, false, false, false);
	}

	public IEnumerator death() {
		float timeOfDeath = Time.time;
		timeForFullDeath = .25f;

		alive = false;

		while (Time.time - timeOfDeath < timeForFullDeath) {
			gameObject.transform.localScale = Vector3.Lerp (new Vector3(1,1,1), new Vector3(0,0,0), (Time.time - timeOfDeath));
//			gameObject.transform.rotation = Quaternion.Euler(0, (Time.time - timeOfDeath) * 360, 0);

			yield return null;
		}

		Destroy (gameObject);
		yield return null;
	}

	public void yourTurn() {
		if (alive) {
			turn90 ();	

			checkForKill ();
		}
	}

	//Rotate facing direction
	public void turn90() {
		//Clockwise
		if (counterClockwise == false) {
			switch (grublinFacingDirection) {
			case Direction.up:
				grublinFacingDirection = Direction.right;
				break;
			case Direction.down:
				grublinFacingDirection = Direction.left;
				break;
			case Direction.left:
				grublinFacingDirection = Direction.up;
				break;
			case Direction.right:
				grublinFacingDirection = Direction.down;
				break;
			}
		}

		//CounterClockwise
		if (counterClockwise == true) {
			switch (grublinFacingDirection) {
			case Direction.up:
				grublinFacingDirection = Direction.left;
				break;
			case Direction.down:
				grublinFacingDirection = Direction.right;
				break;
			case Direction.left:
				grublinFacingDirection = Direction.down;
				break;
			case Direction.right:
				grublinFacingDirection = Direction.up;
				break;
			}
		}

		//Update walk cycle
		spriteToAnimate.updateCycle(grublinFacingDirection, false, false, false);
	}



	void checkForKill() {
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
			gameObject.transform.position.y + 8, 
			gameObject.transform.position.z);

		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;
		string hitObjectType = "";

		Vector3 facingVector = forwardVector();

//		Debug.DrawLine (currentPos, facingVector, Color.white);
		if (Physics.Raycast (currentPos, facingVector, out hit, gameControllerScript.gridSize)) {
			hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
		}

		if (hitObjectType == "SheDwarf") {
			MasterAudio.FireCustomEvent("grublinKills", gameObject.transform.position);
			hit.transform.GetComponent<sheDwarfMove> ().killedByEnemy ();
		}
//		endTurn();
	}

	Vector3 forwardVector() {
		Vector3 returnVector = new Vector3();
		switch (grublinFacingDirection) {
		case Direction.left:
			returnVector = Vector3.left;
			break;
		case Direction.right:
			returnVector = Vector3.right;
			break;
		case Direction.up:
			returnVector = Vector3.forward;
			break;
		case Direction.down:
			returnVector = Vector3.back;
			break;
		}
		return returnVector;
	}
}
