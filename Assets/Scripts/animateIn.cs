﻿using UnityEngine;
using System.Collections;

public class animateIn : MonoBehaviour {

	//Will use for a random time offset
	[HideInInspector]
	public float randomOffset;
	[HideInInspector]
	public float aliveTime;

	// Use this for initialization
	void Start () {
		gameObject.transform.localScale = new Vector3 (0, 0, 0);
		randomOffset = Random.Range (.5f, 1f);
		aliveTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time - randomOffset > aliveTime && gameObject.transform.localScale.y <= 1.0f) {
			gameObject.transform.localScale += new Vector3 (.1f, .1f, .1f);
		}
	}
}
