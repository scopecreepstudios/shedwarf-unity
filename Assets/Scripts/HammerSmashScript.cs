﻿using UnityEngine;
using System.Collections;

public class HammerSmashScript : MonoBehaviour {

	public Sprite[] impactSprites;

	//General speed variable
	public float speedOfAnimation;

	public Direction smashDirection;

	// Use this for initialization
	void Start () {
		speedOfAnimation = .05f;


		StartCoroutine(cycle(impactSprites, speedOfAnimation));

		switch(smashDirection) {
		case Direction.left:
			gameObject.transform.position += new Vector3(-32, 0, 0);
			break;
		case Direction.right:
			gameObject.transform.position += new Vector3(32, 0, 0);
			break;
		case Direction.up:
			gameObject.transform.position += new Vector3(0, 0, 32);
			break;
		case Direction.down:
			gameObject.transform.position += new Vector3(0, 0, -32);
			break;
		}
	}

	//Using this for shedwarf sprite cycles
	public IEnumerator cycle(Sprite[] cycleToRun, float speed) {


		int i = 0;
		while(i < impactSprites.Length) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];
			yield return new WaitForSeconds(speed);

			if(i == 0) {
				GameObject impact = Instantiate(Resources.Load("SoundClouds/SoundBAM"), gameObject.transform.position, gameObject.transform.localRotation) as GameObject;
				impact.name = "SoundBAM";
			}

			i++;
		}
		yield return false;
		destroyIt();
	}

	public void destroyIt() {
		Destroy(gameObject);
	}

}
