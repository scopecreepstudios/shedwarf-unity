﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	//Main Game Controller Script
	[HideInInspector] public GameControllerScript gameControllerScript;

	// Use this for initialization
	void Start () {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	public IEnumerator enemiesAct() {
		GameObject[] grublins = GameObject.FindGameObjectsWithTag("Grublin");
		for(int i = 0; i < grublins.Length; i++) {
			grublins[i].GetComponent<GrublinScript>().yourTurn();
			yield return new WaitForSeconds (.01f);
		}
		GameObject[] skulls = GameObject.FindGameObjectsWithTag ("Skullista");
		for (int i = 0; i < skulls.Length; i++) {
			skulls [i].GetComponent<SkullistaScript> ().shootArrow();
		}
		GameObject[] owls = GameObject.FindGameObjectsWithTag("Owl");
		for(int i = 0; i < owls.Length; i++) {
			owls[i].GetComponent<OwlScript>().yourTurn();
			yield return new WaitForSeconds (.01f);
		}
		GameObject[] mummys = GameObject.FindGameObjectsWithTag ("Mummy");
		for (int i = 0; i < mummys.Length; i++) {
			mummys [i].GetComponent<MummyScript> ().mummyAction ();
			if (mummys [i].GetComponent<MummyScript> ().resting == true) {
				yield return new WaitForSeconds (.01f);
			}
		}

		//TODO Need to wait until everyone is done with movement before we advance
		//For now just wait
//		yield return new WaitForSeconds(.5f);
		gameControllerScript.enemyActionsDone ();
	}
}
