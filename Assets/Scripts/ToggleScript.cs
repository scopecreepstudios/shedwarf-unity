﻿using UnityEngine;
using System.Collections;

public enum ActionsToTrigger {
	Removes,
	Moves,
	Changes
}

public class ToggleScript : MonoBehaviour {

	[HideInInspector] public ActionsToTrigger actionToTrigger;

	private ToggleableObjectScript[] objectsToModify;

	public string toggleName;

	public void OnTriggerEnter(Collider other) {
		Debug.Log(other.gameObject.name);
//		When She Dwarf Enters
		if (other.gameObject.GetComponent<levelObject>().name == "SheDwarf" || other.gameObject.GetComponent<levelObject>().name == "Mummy") {
			toggleTriggered();
		}
	}

	public void toggleTriggered() {

		objectsToModify = GameObject.FindObjectsOfType<ToggleableObjectScript>();

		//Later this should do one of the actions rather than just removes
		switch(actionToTrigger) {
		case ActionsToTrigger.Removes:
			StartCoroutine(removeOneByOne());
			break;
		case ActionsToTrigger.Moves:
			foreach(ToggleableObjectScript objectToModify in objectsToModify) {
				objectToModify.toggleMoves(toggleName);
			}
			break;
		case ActionsToTrigger.Changes:
			foreach(ToggleableObjectScript objectToModify in objectsToModify) {
				objectToModify.toggleChanges(toggleName);
			}
			break;
		}	
	}

	public IEnumerator removeOneByOne() {
		foreach(ToggleableObjectScript objectToModify in objectsToModify) {
			objectToModify.toggleRemoves(toggleName);
			yield return new WaitForSeconds(.01f);
		}
	}
}
