﻿using UnityEngine;
using System.Collections;

public class RotateFloatAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate(Vector3.up * Time.deltaTime * 100);
		gameObject.transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time, 8), gameObject.transform.position.z);
	}
}
