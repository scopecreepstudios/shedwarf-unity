﻿using UnityEngine;
using System.Collections;

public class SkullistaScript : MonoBehaviour {

	//Death Variables
	public bool alive = true;
	public float timeOfDeathStart;
	public float timeForFullDeath = 1.0f;
	public float speed = 4f;

	public GameObject arrow;

	public Direction skullFacingDirection;

	public AnimateSprite spriteToAnimate;

	// Use this for initialization
	void Start () {
//		gameObject.GetComponent<RotateScript>().setStartFacingDirection (skullFacingDirection);
		spriteToAnimate.updateCycle(skullFacingDirection, false, false, false);
	}
	
	// Update is called once per frame
	void Update () {
		if (!alive) {
			if(Time.time - timeOfDeathStart < timeForFullDeath) {
				gameObject.transform.localScale = Vector3.Lerp (new Vector3(1,1,1), new Vector3(0,0,0), (Time.time - timeOfDeathStart));
				gameObject.transform.rotation = Quaternion.Euler(0, (Time.time - timeOfDeathStart) * 360, 0);
			}
			if(Time.time - timeOfDeathStart > timeForFullDeath) {
				death ();
			}
		}
//		gameObject.GetComponent<RotateScript>().setStartFacingDirection (skullFacingDirection);
	}

	//Given a direction return which angle to shoot projectile at
	//TODO figure out why this seems to be rotated by 3/4 of a turn
	public Quaternion shootQuaternion(Direction gameObjectDirection) {
		Quaternion returnDirection = Quaternion.Euler(0, 45, 0);
		switch (gameObjectDirection) {
			case Direction.up:
				returnDirection = Quaternion.Euler(0, 0, 0);
				break;
			case Direction.down:
				returnDirection =  Quaternion.Euler(0, 180, 0);
				break;
			case Direction.left:
				returnDirection = Quaternion.Euler(0, 270, 0);
				break;
			case Direction.right:
				returnDirection = Quaternion.Euler(0, 90, 0);
				break;
		}
		return returnDirection;
	}

	//This should ideally be to the edge of the level
	public int arrowRange = 300;

	public void shootArrow() {
		
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
			gameObject.transform.position.y + 8, 
			gameObject.transform.position.z);

		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;
		string hitObjectType = "";

		Vector3 facingVector = gameObject.GetComponent<RotateScript>().forwardVector(skullFacingDirection);

		Debug.DrawLine (currentPos, facingVector, Color.white);
		if (Physics.Raycast (currentPos, facingVector, out hit, arrowRange)) {
			hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
		}

		if (hitObjectType == "SheDwarf") {
			hit.transform.GetComponent<sheDwarfMove>().killedByEnemy();

			GameObject newArrow = Instantiate (arrow, gameObject.transform.position, Quaternion.identity) as GameObject;

			newArrow.GetComponent<arrowScript>().updateArrowDirection(skullFacingDirection);
		}
	}

	void death() {
		Debug.Log("Dying");
		alive = true;
		Destroy (gameObject);
	}
}
