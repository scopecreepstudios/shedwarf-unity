﻿using UnityEngine;
using System.Collections;

public class arrowScript : MonoBehaviour {

	public float speedOfArrow = 5f;

	public Direction arrowMovementDirection;

	public SpriteRenderer arrowSprite;

	public void updateArrowDirection (Direction newDirection) {
		arrowMovementDirection = newDirection;

		switch(arrowMovementDirection) {
		case Direction.down:
			arrowSprite.gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
			arrowSprite.flipX = false;
			break;
		case Direction.up:
			arrowSprite.gameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
			arrowSprite.flipX = true;
			break;
		case Direction.left:
			arrowSprite.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
			arrowSprite.flipX = false;
			break;
		case Direction.right:
			arrowSprite.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
			arrowSprite.flipX = true;
			break;
		}
	}

	// Update is called once per frame
	void Update () {
		switch(arrowMovementDirection) {
		case Direction.down:
			gameObject.transform.Translate(Vector3.back * speedOfArrow);
			break;
		case Direction.up:
			gameObject.transform.Translate(Vector3.forward * speedOfArrow);
			break;
		case Direction.left:
			gameObject.transform.Translate(Vector3.left * speedOfArrow);
			break;
		case Direction.right:
			gameObject.transform.Translate(Vector3.right * speedOfArrow);
			break;

		}



	}
}
