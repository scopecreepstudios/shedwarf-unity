﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;

public class levelBuild : MonoBehaviour {

	//What starting cell should we use for building the level
	public Vector3 startCell;

	//Main Game Controller Script
	[HideInInspector] public GameControllerScript gameControllerScript;
	[HideInInspector] public PlayerProgressScript playerProgress;

	void Awake () {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
		playerProgress = gameObject.GetComponent<PlayerProgressScript>();
	}

	void Start () {
		Debug.Log("Player Progress Level: " + playerProgress.currentLevelLoad().ToString());
		buildLevel(Resources.Load("LevelCSVs/" + playerProgress.currentLevelLoad()) as TextAsset);
	}

	//Serializes the CSV for our uses
	public void buildLevel(TextAsset levelToLoad) {

		string loadedLevel = levelToLoad.text;

		string[] rows = loadedLevel.Split ('\n');

		for(int i = 1; i < rows.Length ; i++) {
			string[] entries = rows [i].Split (',');

			for (int j = 0; j < entries.Length; j++) {

				Vector3 currentLocation = findLocationOffset(new Vector3 (j * gameControllerScript.gridSize, 0, i * gameControllerScript.gridSize * -1));

				//TODO: do we need this for real?
				//If there's nothing there let's put a space so the switch statement doesn't freak out
				if (entries [j].Length == 0) {
					entries [j] = " ".ToString ();
				}

				//TODO: perhaps this should merely call functions on the class itself that instantiates it at the right place
				switch (entries [j] [0].ToString ()) {
				//SHEDWARF!
				case "p":
					sheDwarfInstantiate (currentLocation, entries [j]);
					break;
				//
				//Enemies
				//
				//Owl
				case "o":
					owlInstantiate(currentLocation, entries[j]);
					break;
				//Grublin
				case "g":
					grublinInstantiate(currentLocation, entries[j]);
					break;
				//Mummy
				case "m":
					mummyInstantiate (currentLocation, entries [j]);
					break;
				//Skullista
				case "s":
					skullistaBlockInstantiate (currentLocation, entries[j]);
					break;
				/*
				//Level Objects
				*/
				//Key
				case "k":
					keyInstantiate (currentLocation, entries [j]);
					break;
					//Door
				case "d":
					//Need extra information for which way the door faces. 
					//Location, entry, row it's in, length of all the rows
					doorInstantiate (currentLocation, entries [j], i, rows.Length);
					break;
					//Toggle
				case "t":
					toggleInstantiate(currentLocation, entries[j]);
					break;
				//Gold
				case "a":
					goldInstantiate (currentLocation, entries [j]);
					break;
				//Chest
				case "c":
					chestInstantiate (currentLocation, entries [j]);
					break;
				//Blocks, can be pushable or static
				case "b":
					blockInstantiate (currentLocation, entries [j]);
					break;
				//
				//Floors and Walls
				//
				//Normal Floor
				case "0":
					floorTile (currentLocation);
					break;
				//Normal Floor
				case " ":
					floorTile (currentLocation);
					break;
				//NOTHINGNESS
				case "x":
					nothingInstantiate (currentLocation, entries [j]);
					break;
				//Wall
				case "w":
					wallInstantiate (currentLocation, entries [j]);
					break;
				default:
					nothingInstantiate (currentLocation, entries [j]);
					Debug.Log ("No Match " + entries [j]);
					break;
				}
			}
		}

		//Do any clean up work
		//Combine all the floor meshes
		GameObject.Find ("FloorParent").GetComponent<CombineMeshScript> ().combineThoseMeshes ();

		//Tell Game Controlle we're done building the level
		gameControllerScript.buildLevelComplete (levelToLoad);
	}


		
	//TODO: this needs to be a lot cleaner and use better tagging. Will break with additional enemy types
	public void destroyAllLevelAssets() {
		
		string[] killableObjects = {"Owl", "Mummy", "Skullista", "Grublin", "Door", "DestroyedOnStart"};

		foreach (string killableObject in killableObjects) {
			GameObject[] killEm = GameObject.FindGameObjectsWithTag (killableObject) as GameObject[];
			foreach (GameObject toKill in killEm) {
				Destroy (toKill.gameObject);
			}	
		}
	}

	//This gets called from GameController when we move to GameTurn.LevelEnd
	public void levelCompleted(TextAsset nextLevelTextAsset) {
		//Clean up level
		destroyAllLevelAssets ();

		//Build the new level
		buildLevel (nextLevelTextAsset);
	}

	//Used to restart level completely
	public void restartLevel() {
		//Clean up level
		destroyAllLevelAssets ();

		//Build the new level
		buildLevel (Resources.Load("LevelCSVs/" + gameObject.GetComponent<PlayerProgressScript>().currentLevelLoad()) as TextAsset);
	}

	public void returnToDungeon() {
		//Clean up level
		destroyAllLevelAssets ();

		//Build the new level
		buildLevel (Resources.Load("LevelCSVs/" + gameObject.GetComponent<PlayerProgressScript>().currentDungeonLoad()) as TextAsset);
	}
		
	public Vector3 findLocationOffset(Vector3 currentLocation) {
		return currentLocation + startCell;
	}

	public void sheDwarfInstantiate(Vector3 location, string entry) {
		floorTile (location);

		//If she doesn't exist we should create her
		if (GameObject.Find ("SheDwarf") == null) {
			GameObject newSheDwarf = Instantiate (Resources.Load ("SheDwarf"), location, Quaternion.identity) as GameObject;
			newSheDwarf.name = "SheDwarf";
		} 

		//Otherwise we should ler her know to get to where she should start
		else {
			GameObject.Find ("SheDwarf").GetComponent<sheDwarfMove> ().levelOverMove (location);
		}
	}

	public void owlInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newOwl = Instantiate(Resources.Load("Owl"), location, Quaternion.identity) as GameObject;
		OwlScript newOwlScript = newOwl.GetComponent<OwlScript>();

		//Facing Starting Direction
		switch (entry[1].ToString ()) {
		case "d":
			newOwlScript.owlFacingDirection = Direction.down;
			break;
		case "u":
			newOwlScript.owlFacingDirection = Direction.up;
			break;
		case "l":
			newOwlScript.owlFacingDirection = Direction.left;
			break;
		case "r":
			newOwlScript.owlFacingDirection = Direction.right;
			break;
		}

		//Set initial start direction
		newOwlScript.spriteToAnimate.updateCycle(newOwlScript.owlFacingDirection, false, false, false);
	}

	public void grublinInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newGrublin = Instantiate(Resources.Load("Grublin"), location, Quaternion.identity) as GameObject;
		GrublinScript newGrublinScript = newGrublin.GetComponent<GrublinScript>();

		//Facing Starting Direction
		switch (entry[1].ToString ()) {
		case "d":
			newGrublinScript.grublinFacingDirection = Direction.down;
			break;
		case "u":
			newGrublinScript.grublinFacingDirection = Direction.up;
			break;
		case "l":
			newGrublinScript.grublinFacingDirection = Direction.left;
			break;
		case "r":
			newGrublinScript.grublinFacingDirection = Direction.right;
			break;
		}

		//Rotation
		switch (entry [2].ToString ()) {
		case "r":
			newGrublinScript.counterClockwise = false;
			break;
		case "l":
			newGrublinScript.counterClockwise = true;
			break;
		}

		//Set initial start direction
		newGrublinScript.spriteToAnimate.updateCycle(newGrublinScript.grublinFacingDirection, false, false, false);

	}

	public void skullistaBlockInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newSkull = Instantiate(Resources.Load("Skullista"), location, Quaternion.identity) as GameObject;

		//Facing Starting Direction
		switch (entry[1].ToString ()) {
		case "d":
			newSkull.GetComponent<SkullistaScript> ().skullFacingDirection = Direction.down;
			break;
		case "u":
			newSkull.GetComponent<SkullistaScript>().skullFacingDirection = Direction.up;
			break;
		case "l":
			newSkull.GetComponent<SkullistaScript> ().skullFacingDirection = Direction.left;
			break;
		case "r":
			newSkull.GetComponent<SkullistaScript> ().skullFacingDirection = Direction.right;
			break;
		default:
			
			break;
		}
	}

	public void mummyInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newMummy = Instantiate(Resources.Load("Mummy"), location, Quaternion.identity) as GameObject;
		newMummy.name = "Mummy";
	}

	public void blockInstantiate(Vector3 location, string entry) {
		floorTile (location);
		if (entry [1].ToString() == "p") {
			GameObject newBlock = Instantiate(Resources.Load("PushableBlock"), location, Quaternion.identity) as GameObject;	
			newBlock.name = "Pushable Block";
		}
		if (entry [1].ToString() == "s") {
			GameObject newBlock = Instantiate(Resources.Load("SmallBrick"), location, Quaternion.identity) as GameObject;	
			newBlock.name = "Small Brick";

			if(entry[2] != null) {

				string toggleName = "";

				for (int p = 2; p < entry.Length; p++) {
					toggleName += entry[p].ToString();
				}

				//Add on a Toggleable Object Script
				newBlock.AddComponent<ToggleableObjectScript>();
				//Set the toggle
				newBlock.GetComponent<ToggleableObjectScript>().toggleName = toggleName;
			}
		}
	}

	public void goldInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newGold = Instantiate(Resources.Load("Gold"), location, Quaternion.identity) as GameObject;
		newGold.name = "Gold";
	}

	public void chestInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newChest = Instantiate(Resources.Load("Chest"), location, Quaternion.identity) as GameObject;
		newChest.name = "Chest";
	}

	public void doorInstantiate(Vector3 location, string entry, int rowNumber, int maxRows) {
		floorTile (location);
		GameObject newDoor = Instantiate(Resources.Load("LockCube"), location- new Vector3(0,0,0), Quaternion.identity) as GameObject;
		//As long as it's not in the first row or last row let's rotate the door 90 degrees so it lines up nicely
		if (rowNumber != 0 && rowNumber != maxRows-1) {
			newDoor.transform.rotation = Quaternion.Euler (0, 90, 0);
		}

		//Grab the rest of the entry as the name of the section transition
		if (entry [1].ToString() != "") {
			string nextLevelName = "";

			for (int p = 1; p < entry.Length; p++) {
				nextLevelName += entry[p].ToString();
			}
			newDoor.GetComponent<DoorScript>().setNextLevelTextAsset(nextLevelName);
		}
	}

	public void toggleInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newToggle = Instantiate(Resources.Load("FloorToggle"), location- new Vector3(0,0,0), Quaternion.identity) as GameObject;

		//TODO make this change depending on the secnd letter entry[1].ToString();
		newToggle.GetComponent<ToggleScript>().actionToTrigger = ActionsToTrigger.Removes;

		//Get the name of the toggle from the rest of the string
		string toggleName = "";
		for (int p = 2; p < entry.Length; p++) {
			toggleName += entry[p].ToString();
		}
		newToggle.GetComponent<ToggleScript>().toggleName = toggleName;
	}
		
	public void keyInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newKey = Instantiate(Resources.Load("Key"), location, Quaternion.identity) as GameObject;
		newKey.name = "Key";
	}

	public void wallInstantiate(Vector3 location, string entry) {
		floorTile (location);
		GameObject newWall = Instantiate(Resources.Load("SampleWall"), location- new Vector3(0,0,0), Quaternion.identity) as GameObject;
		newWall.gameObject.transform.parent = GameObject.Find ("WallParent").transform;
	}

	public void nothingInstantiate(Vector3 location, string entry) {
		GameObject newNothing = Instantiate(Resources.Load("EmptySpace"), location, Quaternion.identity) as GameObject;
		newNothing.name = "Nothing";
	}

	public void floorTile(Vector3 location) {
		GameObject newTile = Instantiate(Resources.Load("StoneFloor"), location - new Vector3(0,0,0), Quaternion.identity) as GameObject;
		newTile.gameObject.transform.parent = GameObject.Find ("FloorParent").transform;
	}
}
