﻿using UnityEngine;
using System.Collections;

public class sconce : MonoBehaviour {

	public Light sconceLight;

	//Rate to walk
	public float flickerRate = 1.0f;

	public bool flick = true;
	// Use this for initialization
	void Start () {
		InvokeRepeating("flicker", flickerRate, flickerRate);
	}
	
	// Update is called once per frame
	void Update () {

	}

	void flicker() {
		if (flick) {
			sconceLight.intensity += -3;
			gameObject.transform.parent.transform.localScale = new Vector3(1,1,1);
			flick = false;
		} else {
			sconceLight.intensity += 3;
			gameObject.transform.parent.transform.localScale = new Vector3(-1,1,1);
			flick = true;
		}
	}

}
