using UnityEngine;
using System.Collections;

public class sheDwarfMove : MonoBehaviour {

	//Facing direction
	public Direction sheDwarfFacing = Direction.up;

	//Is she moving or not
	public bool onTheMove = false;

	//Speed modifier for movement
	public float speed = 10.0f;

	//Main Game Controller Script
	[HideInInspector] public GameControllerScript gameControllerScript;

	//Health
	public bool alive = true;
	public float timeOfDeathStart;
	public float timeForFullDeath = 1.0f;

	public AnimateSheDwarf spriteToAnimate;

	void Awake () {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	void Start () {
		StartCoroutine(Camera.main.GetComponent<animateCamera>().initialMoveIn(gameObject.transform.position));
		alive = true;
	}

	void Update () {
//		Debug.Log (gameObject.transform.position);
		
		if(alive && gameControllerScript.currentTurn == GameTurn.SheDwarfStart) {
			//If she's not moving and direction she should be moving is none
			//Then we can grab inputs/keypresses
			if (!onTheMove) {
				if (Input.GetKeyDown (KeyCode.LeftArrow)) {
					sheDwarfFacing = Direction.left;
					if(objectCheck(shootRays(sheDwarfFacing), sheDwarfFacing)) {
						StartCoroutine (movePlayer(gameObject.transform.position, gameObject.transform.position + new Vector3(-gameControllerScript.gridSize,0,0)));
					}
					else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
						spriteToAnimate.updateCycle(sheDwarfFacing, false, false, false);
						appropriateAction();

					}
				}
				if (Input.GetKeyDown (KeyCode.RightArrow)) {
					sheDwarfFacing = Direction.right;
					if(objectCheck(shootRays(sheDwarfFacing), sheDwarfFacing)) {
						StartCoroutine (movePlayer(gameObject.transform.position, gameObject.transform.position + new Vector3(gameControllerScript.gridSize,0,0)));
					}
					else if (Input.GetKeyDown (KeyCode.RightArrow)) {
						spriteToAnimate.updateCycle(sheDwarfFacing, false, false, false);
						appropriateAction();
					}
				}
				if (Input.GetKeyDown (KeyCode.DownArrow)) {
					sheDwarfFacing = Direction.down;
					if(objectCheck(shootRays(sheDwarfFacing), sheDwarfFacing)) {
						StartCoroutine (movePlayer(gameObject.transform.position, gameObject.transform.position + new Vector3(0,0,-gameControllerScript.gridSize)));
					}
					else if (Input.GetKeyDown (KeyCode.DownArrow)) {
						spriteToAnimate.updateCycle(sheDwarfFacing, false, false, false);
						appropriateAction();

					}
				}
				if (Input.GetKeyDown (KeyCode.UpArrow)) {
					sheDwarfFacing = Direction.up;
					if(objectCheck(shootRays(sheDwarfFacing), sheDwarfFacing)) {
						StartCoroutine (movePlayer(gameObject.transform.position, gameObject.transform.position + new Vector3(0,0,gameControllerScript.gridSize)));
					}
					else if (Input.GetKeyDown (KeyCode.UpArrow)) {
						spriteToAnimate.updateCycle(sheDwarfFacing, false, false, false);
						appropriateAction();
					}
				}
	
				//Actions
				if(Input.GetKeyDown(KeyCode.Space)) {
					appropriateAction();
				}

				//Actions
				if(Input.GetKeyDown(KeyCode.A)) {
					attackWithHammer();
				}
			}
		}
	}

	public void attackWithHammer() {
		spriteToAnimate.updateCycle(sheDwarfFacing, false, true, false);
	}

	//Handles moving the player from one spot to the next
	public IEnumerator movePlayer(Vector3 startPos, Vector3 endPos) {
		
		//Locks other inputs from taking over
		onTheMove = true;

		//Current Position should start at start position and will slowly move towards end position
		Vector3 currentPos = gameObject.transform.position;

		//When does all this start?
		float movementStart = Time.time;

		spriteToAnimate.updateCycle(sheDwarfFacing, true, false, false);

		//Move her from point A to point B in increments of 1 pixel
		while(currentPos != endPos) {
			//Recheck this every frame
			currentPos = gameObject.transform.position;

			Vector3 updatedPos = Vector3.Lerp (startPos, endPos, (Time.time - movementStart) * 3);
			//Lock to a pixel perfect position
			gameObject.transform.position = new Vector3(Mathf.Round(updatedPos.x), Mathf.Round(updatedPos.y), Mathf.Round(updatedPos.z));

			//Return back to this spot
			yield return false;
		}

		//When we're done, release her to move again
		onTheMove = false;

		spriteToAnimate.updateCycle(sheDwarfFacing, false, false, false);

//		StartCoroutine(Camera.main.GetComponent<animateCamera>().moveCamera(gameObject.transform.position));

		//And end the turn
		endTurn();
	}

	//TODO make this so that it works better. 
	//Right now I'm inserting a second pause in there to complete the movement coroutine and then moving again
	public void levelOverMove(Vector3 newLocation) {
		Debug.Log ("Level Over Move" + newLocation);
		onTheMove = false;
//		StartCoroutine (movePlayer (gameObject.transform.position, newLocation));
		gameObject.transform.position = newLocation;
//		StartCoroutine(Camera.main.GetComponent<animateCamera>().moveCamera(gameObject.transform.position));
	}


	void appropriateAction() {
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
		                                  gameObject.transform.position.y + 8, 
		                                  gameObject.transform.position.z);
		
		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;
		string hitObjectType = "";

		Vector3 facingVector = forwardVector(sheDwarfFacing);

		if (Physics.Raycast (currentPos, facingVector, out hit, gameControllerScript.gridSize)) {
			hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
		}

		//TODO have these trigger a function on the objects themselves that handle death
		if (hitObjectType == "Owl") {
			StartCoroutine(hit.transform.GetComponent<OwlScript> ().death ());
			endTurn();
		}

		if (hitObjectType == "Grublin") {
			StartCoroutine(hit.transform.GetComponent<GrublinScript> ().death ());
			endTurn();
		}

		if (hitObjectType == "Mummy") {
			hit.transform.GetComponent<MummyScript>().alive = false;
			hit.transform.GetComponent<MummyScript>().timeOfDeathStart = Time.time;
			endTurn();
		}
		
		if (hitObjectType == "Skull") {
			//You can't kill skulls, silly
			endTurn();
		}

		if (hitObjectType == "Chest") {
			hit.transform.GetComponent<chest>().openUpChest();
			endTurn();
		}

		//Trigger animation
		spriteToAnimate.updateCycle(sheDwarfFacing, false, true, false);
	}

	Vector3 forwardVector(Direction gameObjectFacing) {
		Vector3 returnVector = new Vector3();
		switch (gameObjectFacing) {
		case Direction.left:
			returnVector = transform.TransformDirection (Vector3.left);
			break;
		case Direction.right:
			returnVector = transform.TransformDirection (Vector3.right);
			break;
		case Direction.up:
			returnVector = transform.TransformDirection (Vector3.forward);
			break;
		case Direction.down:
			returnVector = transform.TransformDirection (Vector3.back);
			break;
		}
		return returnVector;
	}

	public GameObject shootRays(Direction rayDirection) {
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
		                                  gameObject.transform.position.y + 8, 
										  gameObject.transform.position.z);

		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;

		//Depending on which direction to move grab the appproriate Vector direction
		Vector3 raycastTransformDirection;

		switch (rayDirection) {
			case Direction.left:
				raycastTransformDirection = transform.TransformDirection (Vector3.left);
				break;
			case Direction.right:
				raycastTransformDirection = transform.TransformDirection (Vector3.right);
				break;
			case Direction.up:
				raycastTransformDirection = transform.TransformDirection (Vector3.forward);
				break;
			case Direction.down:
				raycastTransformDirection = transform.TransformDirection (-Vector3.forward);
				break;
			default:
				raycastTransformDirection = transform.TransformDirection (-Vector3.forward);
				break;
		}

		//Raycast out a gridSize length in the approprate Vector
		if (Physics.Raycast (currentPos, raycastTransformDirection, out hit, gameControllerScript.gridSize)) {
			return hit.transform.gameObject as GameObject;
		} 
		else {
			return this.gameObject;
		}
	}

	public bool objectCheck(GameObject hitObject, Direction directionPressed) {
		string hitObjectType = hitObject.GetComponent<levelObject> ().objectType as string;

		if (hitObjectType != "") {
			//Uncomment this to show what we're running into
//			Debug.Log (hitObjectType);
		}

		//Based on what we're hitting do stuff
		//TODO: wow does this need to not be a huge list, should get reduced to a few main types
		if (hitObjectType == "Wall" ||
			hitObjectType == "Door" ||
			hitObjectType == "Chest" ||
			hitObjectType == "Brick" ||
			hitObjectType == "Grublin" ||
			hitObjectType == "Owl" ||
			hitObjectType == "Skull" ||
			hitObjectType == "Mummy") {
//			endTurn ();
			return false;
		} 
		else if(hitObjectType == "Pushable") {
			return hitObject.GetComponent<PushableBlock>().BeingPushed(directionPressed);
		}
		else if (hitObjectType == "OpenDoor") {
			return true;
		} 
		else {
			return true;
		}
	}

	public void killedByEnemy() {
		StartCoroutine (death ());
	}
		
	public IEnumerator death() {
		//Call this to notify gameController script that you're done for
		gameControllerScript.sheDwarfDied ();

		//Set Alive to false
//		alive = false;
		//Destroy the box collider on She Dwarf so nothing else can happen
		Destroy (gameObject.GetComponent<BoxCollider> ());

		//Start the death coroutine
		float timeOfDeath = Time.time;

		//TODO replace this with the actual death animation
		while (Time.time - timeOfDeath < timeForFullDeath) {
			gameObject.transform.localScale = Vector3.Lerp (new Vector3(1,1,1), new Vector3(0,0,0), (Time.time - timeOfDeath));
			gameObject.transform.rotation = Quaternion.Euler(0, (Time.time - timeOfDeath) * 360, 0);

			yield return null;
		}
			
		//TODO shedwarf's death triggers end of level whatnot
//		alive = true;
//		Destroy (gameObject);
	}

	public void sheDwarfTurnStart() {
		//Move Camera
	}
		
	public void endTurn() {
		gameControllerScript.addMovementNumber ();

		gameControllerScript.sheDwarfTurnEnd();
	}
}