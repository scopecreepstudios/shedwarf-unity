﻿using UnityEngine;
using System.Collections;

//Need to add this in to do custom audio events
using DarkTonic.MasterAudio;

public class PushableBlock : MonoBehaviour {
	
	public int sheDwarfHalfHeight = 16;
	public int rayLength = 32;

	public GameControllerScript gameControllerScript;

	void Awake() {
		gameControllerScript = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}
	
	public bool BeingPushed(Direction pushDirection) {
		
		
		//Current Postion
		Vector3 currentPos = new Vector3 (gameObject.transform.position.x, 
		                                  gameObject.transform.position.y + sheDwarfHalfHeight, 
		                                  gameObject.transform.position.z);

		//Will be used to return any raycasthit information in switch statement
		RaycastHit hit;
		string hitObjectType = "";
		
		//Depending on which direction she wants to move do the appropriate raycast for the block
		switch (pushDirection) {
		case Direction.left:
			Vector3 left = transform.TransformDirection (Vector3.left);

			if (Physics.Raycast (currentPos, left, out hit, rayLength)) {
				hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
			}
			break;

		case Direction.right:
			Vector3 right = transform.TransformDirection (Vector3.right);

			if (Physics.Raycast (currentPos, right, out hit, rayLength)) {
				hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
			}

			break;

		case Direction.up:
			Vector3 up = transform.TransformDirection (Vector3.forward);

			if (Physics.Raycast (currentPos, up, out hit, rayLength)) {
				hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
			}
			break;
		
		case Direction.down:
			Vector3 down = transform.TransformDirection (-Vector3.forward);
			
			if (Physics.Raycast (currentPos, down, out hit, rayLength)) {
				hitObjectType = hit.transform.GetComponent<levelObject>().objectType;
			}
			break;
		}
		
		Debug.Log("PUSHED: " + pushDirection + " " + hitObjectType);		

		if(hitObjectType != "") {
			//Stop her from moving
			return false;
		}
		else {
			//Fire off the Custom Event
			MasterAudio.FireCustomEvent("blockPush", gameObject.transform.position);

			switch (pushDirection) {

			case Direction.left:
				gameObject.transform.Translate(Vector3.left * gameControllerScript.gridSize); 				
				break;
	
			case Direction.right:			
				gameObject.transform.Translate(Vector3.right * gameControllerScript.gridSize);
				break;
	
			case Direction.up:
				gameObject.transform.Translate(Vector3.forward * gameControllerScript.gridSize);
				break;
			
			case Direction.down:
				gameObject.transform.Translate(-Vector3.forward * gameControllerScript.gridSize);
				break;
			}
			
			//Let her move
			return true;
		}
	}
}