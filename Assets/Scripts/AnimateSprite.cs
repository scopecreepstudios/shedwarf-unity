﻿using UnityEngine;
using System.Collections;



//Handles all the visual information for the parent game object, which handles all the logic.
//This should just be told what to display, no logic beyond handling switching between states
public class AnimateSprite : MonoBehaviour {

	[Header("Sprite Sheets")]

	//Idle
	public Sprite[] towardIdle;
	public Sprite[] awayIdle;
	public Sprite[] perpIdle;

	//Attacking
	public Sprite[] towardAttack;
	public Sprite[] awayAttack;
	public Sprite[] perpAttack;

	[Header("Other Variables")]
	//Enum for which animation state we're in
	public AnimState objAnim;

	//General speed variable
	//TODO might have multiples if attack speed should change
	public float speedOfAnimation;

	// Use this for initialization
	void Start () {
		speedOfAnimation = .05f;
//		StartCoroutine(idleCycle(towardIdle, speedOfAnimation));	
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void updateCycle(Direction directionOfSprite, bool moving, bool attacking, bool death) {
		StopAllCoroutines();

		switch(directionOfSprite) {
		case Direction.up:
			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(awayIdle, speedOfAnimation));
			}
			break;

		case Direction.down:
			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(towardIdle, speedOfAnimation));
			}
			break;

		case Direction.left:
			gameObject.GetComponent<SpriteRenderer>().flipX = true;
			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(perpIdle, speedOfAnimation));
			}
			break;

		case Direction.right:
			gameObject.GetComponent<SpriteRenderer>().flipX = false;
			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(perpIdle, speedOfAnimation));
			}
			break;
		}
	}

	//Using this for idle sprite cycles
	public IEnumerator idleCycle(Sprite[] cycleToRun, float speed) {

		int i = 0;
		while(i != -1) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];
			yield return new WaitForSeconds(speed);

			i++;
			if(i >= cycleToRun.Length) {
				i = 0;
			}
		}
		yield return false;
	}

	//Attack!
	public IEnumerator attackCycle(Sprite[] cycleToRun, Sprite[] weaponCycleToRun, float speed, Direction followUpDirection) {

		int i = 0;

		while(i < cycleToRun.Length) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];

			yield return new WaitForSeconds(speed);

			//Do we need to start something else during the animation? do it here
			if(i == 4) {
				
			}
			i++;
		}
			

		//After the swing what she should do
		updateCycle(followUpDirection, false, false, false);
		yield return false;
	}
}
