﻿using UnityEngine;
using System.Collections;

public class animateCamera : MonoBehaviour {

	//How far back from Shedwarf should we end up?
	public Vector3 cameraPosition;

	//Final Rotation of camera
	public Vector3 finalRotation;

	//Speed multiplier for the Lerp
	public float speed = 200.0F;
	//When did we start this object
	private float startTime;
	//Use this for determining the length of the journey.
	private float journeyLength;

	// Use this for initialization
	void Start () {
		
//		StartCoroutine(initialMoveIn());
	}

	//Called when shedwarf shows up
	public IEnumerator initialMoveIn(Vector3 moveToPosition) {

		//Set Start time to the time the object is created
		startTime = Time.time;

		//How far is the distance between the where we'll start and where we'll go
		journeyLength = Vector3.Distance(new Vector3(0,0,0), moveToPosition + cameraPosition);

		//Frac Journey is the distance covered / journey length. This should always be between 0 and 1
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;

		//Do the lerp. Go between start and end based on how far into the journey we should be based on time elapsed
		while(gameObject.transform.position != moveToPosition + cameraPosition) {
			transform.rotation = Quaternion.Euler(Vector3.Lerp (new Vector3 (0,90,0), finalRotation, fracJourney));
			transform.position = Vector3.Lerp(new Vector3(0,0,0), moveToPosition + cameraPosition, fracJourney);

			//Distance Covered is the current time less when we starrted multiplied by speed
			distCovered = (Time.time - startTime) * speed;
			//Frac Journey is the distance covered / journey length. This should always be between 0 and 1
			fracJourney = distCovered / journeyLength;

			yield return null;
		}

		StartCoroutine(stayWithSheDwarf());
	}
		
	public bool lockMovement;

	public IEnumerator moveCamera(Vector3 moveToPosition) {
		if (!lockMovement) {
			lockMovement = true;	
			Vector3 endPosition = moveToPosition + cameraPosition;

			float startTime = Time.time;

			while (gameObject.transform.position != endPosition) {
				
				gameObject.transform.position = Vector3.Lerp (gameObject.transform.position, endPosition, (Time.time - startTime) * 1f);

				yield return null;
			}

			lockMovement = false;
		}

		StartCoroutine(stayWithSheDwarf());
	}

	public IEnumerator stayWithSheDwarf() {
		GameObject sheDwarf = GameObject.Find("SheDwarf");
		while (true) {
			gameObject.transform.position = sheDwarf.transform.position + cameraPosition;
			yield return null;
		}
	}
}
