﻿using UnityEngine;
//Needed for AssetDatabase stuff
using UnityEditor;

using System.Collections;

using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class PlayerProgressScript : MonoBehaviour {

	[Header("Save Load Information")]
	public TextAsset saveLoad;

	public XmlDocument doc;



	// Use this for initialization
	void Awake () {
		Load(saveLoad);
	}

	//Get the strings from Dungeons, sections, levels
	//TODO: maybe combine these together
	public string currentDungeonLoad() {
		return doc.SelectSingleNode("//CurrentDungeon").InnerText;
	}

	public string currentSectionLoad() {
		return doc.SelectSingleNode("//CurrentSection").InnerText;
	}

	public string currentLevelLoad() {
		return doc.SelectSingleNode("//CurrentLevel").InnerText;
	}
		
	//Save out the current Dungeon, Section, Level
	//TODO: maybe combine these together
	public void currentDungeonUpdate(string infoToSave) {
		XmlNode currentElement = doc.SelectSingleNode("//CurrentDungeon");
		currentElement.InnerText = infoToSave;
	}

	public void currentSectionUpdate(string infoToSave) {
		XmlNode currentElement = doc.SelectSingleNode("//CurrentSection");
		currentElement.InnerText = infoToSave;
	}

	public void currentLevelUpdate(string infoToSave) {
		XmlNode currentElement = doc.SelectSingleNode("//CurrentLevel");
		currentElement.InnerText = infoToSave;
	}


	//Finds the position of a toggle
	public string togglePosition(string nameOfToggle) {
		//Get the node
		XmlElement toggle = doc.SelectSingleNode("//Toggle[@name='" + nameOfToggle + "']") as XmlElement;

		//Sets the attribute...testing
		toggle.SetAttribute("flipped", "true");

		//Gets the attribute
		string position = toggle.GetAttribute("flipped");

		//Returns it so we can do what we need to with it
		return position;
	}

	//Load the XML document
	public void Load(TextAsset documentToLoad) {

		//http://answers.unity3d.com/questions/472352/force-unity-to-refresh-a-text-asset-resources-fold.html
		//Reimport when we're loading to make sure we're using the latest version of the file
		//Unity should save out just fine, it's loading we're having issues with
		AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(saveLoad));

		doc = new XmlDocument();
		doc.LoadXml(documentToLoad.text);
	}


	//Save the XML document
	public void Save() {
		//Check for existence of save directory
		if(!Directory.Exists("Assets/SaveData")) {
			//If it doesn't exist, let's create it
			Directory.CreateDirectory("Assets/SaveData");	
		}

		doc.Save("Assets/SaveData/testing.xml");

		Debug.Log("Data Saved");
	}
}
