﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CombineMeshScript : MonoBehaviour {
	void Start() {
//		combineThoseMeshes ();
	}

	public void combineThoseMeshes() {
		MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combine = new CombineInstance[meshFilters.Length];
		int i = 0;

		while (i < meshFilters.Length) {
			combine[i].mesh = meshFilters[i].sharedMesh;
			combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
			if (meshFilters [i].name != "FloorParent") {
				meshFilters [i].gameObject.SetActive (false);
//				Destroy(meshFilters[i].gameObject);
			}
			i++;
		}
		transform.GetComponent<MeshFilter>().mesh.Clear ();
		transform.GetComponent<MeshFilter>().mesh = new Mesh();
		transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
		transform.gameObject.SetActive (true);
	}

	public void cleanUpAtLevelEnd() {
		foreach (Transform child in transform) {
			Destroy (child.gameObject);
		}
	}
}