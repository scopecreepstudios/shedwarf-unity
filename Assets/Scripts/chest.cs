﻿using UnityEngine;
using System.Collections;

public class chest : MonoBehaviour {

	public GameObject openChest;
	public GameObject closedChest;

	public GameObject treasure;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void openUpChest() {
		closedChest.SetActive (false);
		Instantiate (treasure, gameObject.transform.position, Quaternion.identity);
	}
}
