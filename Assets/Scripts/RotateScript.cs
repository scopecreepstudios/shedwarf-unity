﻿using UnityEngine;
using System.Collections;

public class RotateScript : MonoBehaviour {

	//Initial Setting of facing Direction
	//Rotate facing direction
	public void setStartFacingDirection(Direction gameObjectFacing) {
		switch (gameObjectFacing) {
		case Direction.up:
			gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
			break;
		case Direction.down:
			gameObject.transform.rotation = Quaternion.Euler(0, 180, 0);
			break;
		case Direction.left:
			gameObject.transform.rotation = Quaternion.Euler(0, 270, 0);
			break;
		case Direction.right:
			gameObject.transform.rotation = Quaternion.Euler(0, 90, 0);
			break;
		}
	}


	//Retruns a vector based on which direction the object is facing
	//Mainly used for determining direction of attack/projecticle
	public Vector3 forwardVector(Direction gameObjectDirection) {
		Vector3 returnVector = new Vector3();
		switch (gameObjectDirection) {
		case Direction.left:
			returnVector = Vector3.left;
			break;
		case Direction.right:
			returnVector = Vector3.right;
			break;
		case Direction.up:
			returnVector = Vector3.forward;
			break;
		case Direction.down:
			returnVector = Vector3.back;
			break;
		}
		return returnVector;
	}
}
