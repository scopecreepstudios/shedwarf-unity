﻿using UnityEngine;
using System.Collections;

public class death : MonoBehaviour {
	
	//Death Variables
	public bool alive = true;
	public float timeOfDeathStart;
	public float timeForFullDeath = 1.0f;
	public float speed = 4f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!alive) {
			if(Time.time - timeOfDeathStart < timeForFullDeath) {
				gameObject.transform.localScale = Vector3.Lerp (new Vector3(1,1,1), new Vector3(0,0,0), (Time.time - timeOfDeathStart));
				gameObject.transform.rotation = Quaternion.Euler(0, (Time.time - timeOfDeathStart)*360, 0);
			}
			if(Time.time - timeOfDeathStart > timeForFullDeath) {
				die ();
			}
		}
	}

	void die() {
		alive = true;
		Destroy (gameObject);
	}
	
}
