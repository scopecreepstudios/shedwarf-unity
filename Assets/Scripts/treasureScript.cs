﻿using UnityEngine;
using System.Collections;

public class treasureScript : MonoBehaviour {

	public float aliveTime;

	// Use this for initialization
	void Start () {
		aliveTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.Translate (new Vector3 (0,1,0));
		gameObject.transform.localScale = Vector3.Lerp (new Vector3(1,1,1), new Vector3(.1f,.1f,.1f), (Time.time - aliveTime));
		if (Time.time - aliveTime > .5f) {
			Destroy (gameObject);
		}
	}
}
