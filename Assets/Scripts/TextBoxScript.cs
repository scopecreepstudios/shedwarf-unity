﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBoxScript : MonoBehaviour {

	public Text textBox;

	public void readSign(string signText) {
		textBox.text = signText;
		StartCoroutine (openSign ());
	}

	public void unReadSign() {
		textBox.text = "";
		StartCoroutine (closeSign ());
	}

	public IEnumerator openSign() {
		CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup> () as CanvasGroup;
		while (canvasGroup.alpha != 1) {
			canvasGroup.alpha += .1f;
			yield return false;
		}
		yield return null;
	}

	public IEnumerator closeSign() {
		CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup> () as CanvasGroup;
		while (canvasGroup.alpha != 0) {
			canvasGroup.alpha -= .1f;
			yield return false;
		}
		yield return null;
	}
}
