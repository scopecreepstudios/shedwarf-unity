﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SignScript : MonoBehaviour {

	public string signText;

	[HideInInspector] public TextBoxScript textBoxScript;

	void Start() {
		textBoxScript = GameObject.Find ("TextBox").GetComponent<TextBoxScript>();	

	}



	void OnTriggerEnter(Collider other) {
		Debug.Log(other.name);
		if (other.GetComponent<levelObject> ().objectType == "SheDwarf") {
			textBoxScript.readSign (signText);
		}
	}

	void OnTriggerExit(Collider other) {
		Debug.Log(other.name);
		if (other.GetComponent<levelObject> ().objectType == "SheDwarf") {
			textBoxScript.unReadSign ();
		}
	}
}
