﻿using UnityEngine;
using System.Collections;

public class SoundFXScript : MonoBehaviour {

	private float aliveTime;
	public float lengthOfAnimation = .5f;

	//Forces class to be serializable
	[System.Serializable]
	public class Options {
		public bool grow = false;
		public bool fadeOut = false;
		public bool floatUp = false;
		public bool floatForward = false;
	}

	[Header("Options for Animations")]
	//Let's make a variable using the options class
	public Options options;

	// Use this for initialization
	void Start () {
		aliveTime = Time.time;
		StartCoroutine(
			animate(lengthOfAnimation)
		);
	}

	public IEnumerator animate(float animationLength) {
		while(Time.time - aliveTime <= animationLength) {
			if(options.floatUp) {
				gameObject.transform.position += new Vector3(0,1f,0);	
			}

			if(options.floatForward) {
				gameObject.transform.position += new Vector3(0,0,-1f);	
			}

			if(options.grow) {
				gameObject.transform.localScale += new Vector3(.1f,.01f,.01f);	
			}

			if(options.fadeOut) {
				gameObject.GetComponent<SpriteRenderer>().color -= new Color(0,0,0,.01f);	
			}
				
			yield return new WaitForFixedUpdate();
		}
		Destroy(gameObject);
	}
}
