﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public enum AnimState {
	toward,
	away,
	perpLeft,
	perpRight,
	dying,
	attack
}

//Handles all the visual information for the parent game object, which handles all the logic.
//This should just be told what to display, no logic beyond handling switching between states
public class AnimateSheDwarf : MonoBehaviour {
		
	[Header("Sprite Sheets")]

	//Walking
	public Sprite[] towardWalk;
	public Sprite[] awayWalk;
	public Sprite[] perpWalk;

	//Idle
	public Sprite[] towardIdle;
	public Sprite[] awayIdle;
	public Sprite[] perpIdle;

	//Attacking
	public Sprite[] towardAttack;
	public Sprite[] awayAttack;
	public Sprite[] perpAttack;

	//Hammer
	public Sprite[] towardHammer;
	public Sprite[] awayHammer;
	public Sprite[] perpHammer;

	[Header("Other Variables")]
	//Enum for which animation state we're in
	public AnimState objAnim;

	//General speed variable
	//TODO might have multiples if attack speed should change
	public float speedOfAnimation;

	// Use this for initialization
	void Start () {
		speedOfAnimation = .05f;
		StartCoroutine(idleCycle(towardIdle, speedOfAnimation));
	}

	//Using this for idle sprite cycles
	public IEnumerator idleCycle(Sprite[] cycleToRun, float speed) {
		
		int i = 0;
		while(i != -1) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];
			yield return new WaitForSeconds(speed);

			i++;
			if(i >= cycleToRun.Length) {
				i = 0;
			}
		}
		yield return false;
	}

	//Using this for shedwarf walking sprite cycles
	public IEnumerator walkCycle(Sprite[] cycleToRun, float speed) {

		int i = 0;
		while(i != -1) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];
			yield return new WaitForSeconds(speed);

			//Fire off the "pts"
			if(i == 1) {
				GameObject ptsound = Instantiate(Resources.Load("SoundClouds/SoundPT"), 
					new Vector3(gameObject.transform.position.x, 8, gameObject.transform.position.z - 16), 
					gameObject.transform.localRotation) as GameObject;
				ptsound.name = "SoundPT";
			}

			//Fire off the "pts"
			if(i == 4) {
				GameObject ptsound = Instantiate(Resources.Load("SoundClouds/SoundPT"), 
					new Vector3(gameObject.transform.position.x + 10, 4, gameObject.transform.position.z - 16), 
					gameObject.transform.localRotation) as GameObject;
				ptsound.name = "SoundPT";
			}

			i++;
			if(i >= cycleToRun.Length) {
				i = 0;
			}
		}
		yield return false;
	}



	//GameObject that references the item slot
	public GameObject sheDwarfItem;

	//Using this for shedwarf's weapons
	//TODO: Tightly coupled to shedwarf's item slot. Might want to change this to be generic
	public IEnumerator weaponCycle(Sprite[] cycleToRun, Sprite[] weaponCycleToRun, float speed, Direction followUpDirection) {

		int i = 0;

		//Hammer sound
		MasterAudio.FireCustomEvent("hammer", gameObject.transform.position);

		while(i < cycleToRun.Length) {
			gameObject.GetComponent<SpriteRenderer> ().sprite = cycleToRun[i];
			sheDwarfItem.GetComponent<SpriteRenderer> ().sprite = weaponCycleToRun[i];

			yield return new WaitForSeconds(speed);

			if(i == 4) {
				GameObject impact = Instantiate(Resources.Load("ImpactSmash"), gameObject.transform.position, gameObject.transform.localRotation) as GameObject;
				impact.GetComponent<HammerSmashScript>().smashDirection = followUpDirection;
			}
			i++;
		}

		sheDwarfItem.GetComponent<SpriteRenderer> ().sprite = null;

		//After the swing what she should do
		updateCycle(followUpDirection, false, false, false);
		yield return false;
	}

	//Runs to update which animation cycles we should be using
	public void updateCycle(Direction directionOfSprite, bool moving, bool attacking, bool death) {
		//Stop current cycle
		StopAllCoroutines();

		switch (directionOfSprite) {
		//UP
		case Direction.up:
			if(attacking) {
				objAnim = AnimState.attack;
				StartCoroutine(weaponCycle(awayAttack, awayHammer, speedOfAnimation, Direction.up));
			}

			if (moving) {
				objAnim = AnimState.away;
				StartCoroutine(walkCycle(awayWalk, speedOfAnimation));	
			}

			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(awayIdle, speedOfAnimation));
			}
			break;
		//DOWN
		case Direction.down:
			if(attacking) {
				objAnim = AnimState.attack;
				StartCoroutine(weaponCycle(towardAttack, towardHammer, speedOfAnimation, Direction.down));
			}

			if (moving) {
				objAnim = AnimState.away;
				StartCoroutine(walkCycle(towardWalk, speedOfAnimation));	
			}

			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(towardIdle, speedOfAnimation));
			}
			break;
		//LEFT
		case Direction.left:
			gameObject.GetComponent<SpriteRenderer>().flipX = true;
			sheDwarfItem.GetComponent<SpriteRenderer>().flipX = true;
			if(attacking) {
				objAnim = AnimState.attack;
				StartCoroutine(weaponCycle(perpAttack, perpHammer, speedOfAnimation, Direction.left));
			}

			if (moving) {
				objAnim = AnimState.away;
				StartCoroutine(walkCycle(perpWalk, speedOfAnimation));	
			}

			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(perpIdle, speedOfAnimation));
			}
			break;
		//RIGHT
		case Direction.right:
			gameObject.GetComponent<SpriteRenderer>().flipX = false;
			sheDwarfItem.GetComponent<SpriteRenderer>().flipX = false;
			if(attacking) {
				objAnim = AnimState.attack;
				StartCoroutine(weaponCycle(perpAttack, perpHammer, speedOfAnimation, Direction.right));
			}
			if (moving) {
				objAnim = AnimState.away;
				StartCoroutine(walkCycle(perpWalk, speedOfAnimation));	
			}
			if (!moving && !attacking) {
				objAnim = AnimState.away;
				StartCoroutine(idleCycle(perpIdle, speedOfAnimation));
			}
			break;
		}
	}
}
