﻿using UnityEngine;
using UnityEditor;

public class SheDwarfHelper : EditorWindow {

	private Vector3 prevPosition;
	private bool doSnap = true;
	private float snapValue = 16;

	[MenuItem( "Edit/She Dwarf Helper %_l" )]
	static void Init() {
		var window = (SheDwarfHelper)EditorWindow.GetWindow( typeof( SheDwarfHelper ) );
		window.maxSize = new Vector2( 300, 600 );
	}


	public void OnGUI() {
		GUILayout.Label ("\nSnap Options");
		doSnap = EditorGUILayout.Toggle( "Auto Snap", doSnap );
		snapValue = EditorGUILayout.FloatField( "Snap Value", snapValue );

		//ENEMIES
		GUILayout.Label ("\nEnemies");
		if (GUILayout.Button ("Grublin")) {
			createNewObj ("Grublin");
		}
		if(GUILayout.Button ("Skull Block")) {
			createNewObj ("Skull Block");
		}
		//WALLS AND BRICKS
		GUILayout.Label ("Walls and Bricks");
		if(GUILayout.Button ("Chest")) {
			createNewObj ("Chest");
		}
		if(GUILayout.Button ("Pushable Block")) {
			createNewObj ("PushableBlock");
		}
		if(GUILayout.Button ("SmallBrick")) {
			createNewObj ("SmallBrick");
		}
		//COLLECTABLES
		GUILayout.Label("Collectables");
		if(GUILayout.Button ("Gold")) {
			createNewObj ("Gold");
		}
		if(GUILayout.Button ("Key")) {
			createNewObj ("Key");
		}


	}

	public void createNewObj(string name) {
		GameObject newObj = Instantiate (Resources.Load(name.ToString())) as GameObject;
		Selection.activeGameObject = newObj.gameObject;
	}


	public void Update() {
		if ( doSnap
			&& !EditorApplication.isPlaying
			&& Selection.transforms.Length > 0
			&& Selection.transforms[0].position != prevPosition )
		{
			Snap();
			prevPosition = Selection.transforms[0].position;
		}

	}

	private void Snap() {
		foreach ( var transform in Selection.transforms ) {
			var t = transform.transform.position;
			t.x = Round( t.x );
			t.y = Round( t.y );
			t.z = Round( t.z );
			transform.transform.position = t;
		}
	}

	private float Round( float input ) {
		return snapValue * Mathf.Round( ( input / snapValue ) );
	}


}