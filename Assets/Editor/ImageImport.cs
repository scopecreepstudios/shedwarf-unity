﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class ImageImport : AssetPostprocessor {

	//Used this as documentation:
	//https://gist.github.com/kamend/6a6dd80b233079b6e101
	void OnPreprocessTexture () {

		//Check if it exists or not
		Object asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(Texture2D));

		if (!asset) {
//			//Get the TextureImporter 
//			TextureImporter import = assetImporter as TextureImporter;
//
//			//Set it to a Sprite
//			import.textureType = TextureImporterType.Sprite;
//
//			//Set to 1 pixel per unit
//			import.spritePixelsPerUnit = 1;
//
//			//Set to multiple since we're likely doing spritesheets
//			import.spriteImportMode = SpriteImportMode.Multiple;
//
//			//Make it not compress and look terrible
//			import.filterMode = FilterMode.Point;
		}
		else {
			
		}
	}
}